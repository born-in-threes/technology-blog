# mybook

> A Vue.js project

## Build Setup

``` bash
# install dependencies
# 安装依赖
npm install

# serve with hot reload at localhost:8080
# 运行在8080端口
npm run dev

# build for production with minification
# 打包
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
